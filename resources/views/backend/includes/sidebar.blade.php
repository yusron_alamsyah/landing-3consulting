<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 " id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0 text-center" href="{{route('dashboard')}}">
        <img src="{{url('backend//assets/img/logo.png')}}" class="navbar-brand-img h-100" alt="main_logo">
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse w-auto " style="height:auto;" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}" href="{{route('dashboard')}}">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-tv-2 text-dark text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a data-bs-toggle="collapse" href="#componentsExamples" class="nav-link collapsed" aria-controls="componentsExamples" role="button" aria-expanded="false">
              <div class="icon icon-shape icon-sm text-center d-flex align-items-center justify-content-center">
                  <i class="ni ni-app text-dark text-sm"></i>
              </div>
              <span class="nav-link-text ms-1">Contens</span>
          </a>
          <div class="collapse {{ (request()->segment(2) == 'content' or request()->segment(2) == 'service' or request()->segment(2) == 'price' or request()->segment(2) == 'feature') ? 'show' : '' }}" id="componentsExamples" style="">
              <ul class="nav ms-4">
                  <li class="nav-item">
                      <a class="nav-link {{ (request()->segment(3) == 'header') ? 'active' : '' }}" href="{{route('content.edit','header')}}">
                          <span class="sidenav-mini-icon"> H </span>
                          <span class="sidenav-normal"> Header </span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link {{ (request()->segment(2) == 'service') ? 'active' : '' }}" href="{{route('service.index')}}">
                          <span class="sidenav-mini-icon"> S </span>
                          <span class="sidenav-normal"> Services </span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link {{ (request()->segment(3) == 'statistic') ? 'active' : '' }}" href="{{route('content.edit','statistic')}}">
                          <span class="sidenav-mini-icon"> S </span>
                          <span class="sidenav-normal"> Statistic </span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link {{ (request()->segment(2) == 'feature') ? 'active' : '' }}" href="{{route('feature.index')}}">
                          <span class="sidenav-mini-icon"> F </span>
                          <span class="sidenav-normal"> Features </span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link {{ (request()->segment(2) == 'price') ? 'active' : '' }}" href="{{route('price.index')}}">
                          <span class="sidenav-mini-icon"> P </span>
                          <span class="sidenav-normal"> Pricing </span>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link {{ (request()->segment(3) == 'contact') ? 'active' : '' }}" href="{{route('content.edit','contact')}}">
                          <span class="sidenav-mini-icon"> C </span>
                          <span class="sidenav-normal"> Contact </span>
                      </a>
                  </li>
              </ul>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ (request()->segment(2) == 'news') ? 'active' : '' }}" href="{{route('news.index')}}">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-calendar-grid-58 text-dark text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">News</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ (request()->segment(2) == 'career') ? 'active' : '' }}" href="#">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-credit-card text-dark text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Career</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ (request()->segment(2) == 'testimoni') ? 'active' : '' }}" href="{{route('testimoni.index')}}">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-app text-dark text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Testimonials</span>
          </a>
        </li>
        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account pages</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ (request()->segment(2) == 'profile') ? 'active' : '' }}" href="{{route('profile')}}">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-single-02 text-dark text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Profile</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ (request()->segment(2) == 'configuration') ? 'active' : '' }}" href="{{route('config')}}">
            <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-settings text-dark text-sm opacity-10"></i>
            </div>
            <span class="nav-link-text ms-1">Setting</span>
          </a>
        </li>
        <li class="nav-item">
          <form action="{{url('logout')}}" method="POST">
            @csrf
            <button class="nav-link" href="javascript:;" type="submit" style="background:transparent;border:none;">
                <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                  <i class="ni ni-collection text-dark text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">Logout</span>
            </a>
          </form>
        </li>
      </ul>
    </div>
  </aside>