<!--   Core JS Files   -->
<script src="{{ url('backend/assets/js/core/popper.min.js') }}"></script>
<script src="{{ url('backend/assets/js/core/bootstrap.min.js') }}"></script>
<script src="{{ url('backend/assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
<script src="{{ url('backend/assets/js/plugins/smooth-scrollbar.min.js') }}"></script>
<script src="{{ url('backend/assets/js/plugins/chartjs.min.js') }}"></script>
<script src="https://kit.fontawesome.com/e20c7cbada.js" crossorigin="anonymous"></script>
<script src="{{ url('backend/assets/js/custom.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 

<script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
        damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
</script>
<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{url('backend/assets/js/argon-dashboard.min.js?v=2.0.4')}}"></script>