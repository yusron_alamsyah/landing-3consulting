<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be include in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ Storage::url($conf_logo) }}">
  <link rel="icon" type="image/png" href="{{ Storage::url($conf_logo) }}">
  <title>@yield('title') - {{$conf_title}}</title>
  <meta name="description" content="{{$conf_desc}}">
  <meta name="author" content="{{$conf_author}}">
  <meta name="keywords" content="{{$conf_keyword}}">
  
  @include('backend.includes.style_admin')

  @stack('styles')

</head>

<body class="g-sidenav-show   bg-gray-100">
  <div class="min-height-300 bg-primary position-absolute w-100"></div>

  @include('backend.includes.sidebar')

  <main class="main-content position-relative border-radius-lg ">
    
    @include('backend.includes.navbar')

    <div class="container-fluid py-4">
  
      @yield('content')

      @include('backend.includes.footer')

    </div>

  </main>
  
  @include('backend.includes.scripts_admin')

  @stack('scripts')

</body>

</html>