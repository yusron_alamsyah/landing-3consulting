@extends('backend.layouts.admin')

@section('title','Add Content '.request()->segment(3))

@section('content')

<div class="row">
    {{-- show error --}}
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show text-white" role="alert">
            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
            <span class="alert-text"><strong>Error!</strong></span>
            <p>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="card mt-4">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <div class="col-12 d-flex align-items-center">
                    <h5 class="font-weight-bolder mb-0">Form Price</h5>
                </div>
            </div>
        </div>
        
        <div class="card-body px-0 pt-0 pb-4">
            <form action="{{route('price.store')}}" method="POST" autocomplete="off">
                @csrf
                <div class="row mx-2">
                    <div class="form-group">
                        <label class="form-control-label">Title</label>
                        <input class="form-control" type="text" value="{{old('title')}}" id="title" name="title">
                    </div>
                </div>
                <div class="row mx-2">
                    <div class="form-group">
                        <label class="form-control-label">Icon</label>
                        <input class="form-control" type="text" value="{{old('icon')}}" id="icon" name="icon" placeholder="lni-cog">
                        <div id="emailHelp" class="form-text">You can see more icons in <a href="https://lineicons.com/icons/" target="_blank">here</a></div>
                    </div>
                </div>
                <div class="row mx-2">
                    <div class="form-group">
                        <label class="form-control-label">Price</label>
                        <div class="input-group">
                            <span class="input-group-text" id="basic-addon1">$</span>
                            <input class="form-control" type="number" value="{{old('price')}}" id="price" name="price">
                            <span class="input-group-text" id="basic-addon2">/mo</span>
                        </div>
                    </div>
                </div>
                <div class="row mx-2">
                    <div class="form-group">
                        <label class="form-control-label">Description</label>
                        <textarea class="form-control my-editor" id="my-editor" name="description" rows="10">{{old('description')}}</textarea>
                    </div>
                </div>
                <div class="row mx-2">
                    <div class="form-group text-end">
                        <a href="{{route('price.index')}}" type="reset" class="btn btn-danger"><i class="fas fa-times mx-1" aria-hidden="true"></i>Cancel</a>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-check mx-1" aria-hidden="true"></i>Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="https://cdn.ckeditor.com/4.20.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('my-editor');
    </script>
@endpush