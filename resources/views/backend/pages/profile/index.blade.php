@extends('backend.layouts.admin')

@section('title','Edit Profile')

@section('content')

<div class="row">
    @if ($message = Session::get('success'))
          <div class="alert alert-success alert-dismissible fade show text-white" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Message</strong></span>
              <p>{{$message}}</p>
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      @endif
    
    <div class="card shadow-lg mx-4 card-profile-bottom">
        <div class="card-body p-3">
          <div class="row gx-4">
            <div class="col-auto">
              <div class="avatar avatar-xl position-relative">
                <img src="https://ui-avatars.com/api?name={{$item->name}}" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
              </div>
            </div>
            <div class="col-auto my-auto">
              <div class="h-100">
                <h5 class="mb-1">
                  {{$item->name}}
                </h5>
                <p class="mb-0 font-weight-bold text-sm">
                  {{$item->email}}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid py-4">
        <div class="row">
          <div class="col-md-12">
            {{-- show error --}}
            @if ($errors->any())
              <div class="alert alert-danger alert-dismissible fade show text-white" role="alert">
                  <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                  <span class="alert-text"><strong>Error!</strong></span>
                  <p>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{$error}}</li>
                          @endforeach
                      </ul>
                  </p>
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
            @endif
            <div class="card">
              <div class="card-header pb-0">
                <div class="d-flex align-items-center">
                  <p class="mb-0">Edit Profile</p>
                </div>
              </div>
              <div class="card-body">
                <form action="{{route('profile-update',$item->id)}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                  @method('PUT')
                  @csrf
                  <p class="text-uppercase text-sm">User Information</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="example-text-input" class="form-control-label">Name</label>
                        <input class="form-control" name="name" id="name" type="text" value="{{$item->name}}">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="example-text-input" class="form-control-label">Email address</label>
                        <input class="form-control" name="email" id="email"  type="email" value="{{$item->email}}">
                      </div>
                    </div>
                  <hr class="horizontal dark">
                  <p class="text-uppercase text-sm">Account Information</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="example-text-input" class="form-control-label">Password</label>
                        <input class="form-control" name="password" id="password" type="password" value="">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="example-text-input" class="form-control-label">Confirm Password</label>
                        <input class="form-control" name="password_confirmation" id="password_confirmation" type="password" value="">
                      </div>
                    </div>
                  </div>
                  <hr class="horizontal dark">
                  <div class="row">
                    <div class="col-md-12 text-end">
                      <button type="submit" class="btn btn-primary"><i class="fas fa-check mx-1" aria-hidden="true"></i>Update</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

@endsection
