@extends('backend.layouts.admin')

@section('title','Content '.request()->segment(3))

@push('styles')
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@section('content')

<div class="row">

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show text-white" role="alert">
            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
            <span class="alert-text"><strong>Message</strong></span>
            <p>{{$message}}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    
    <div class="card mt-4">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <div class="col-6 d-flex align-items-center">
                    <h5 class="font-weight-bolder mb-0">Data Service</h5>
                </div>
                <div class="col-6 text-end">
                    <a class="btn bg-gradient-dark mb-0" href="{{route('service.create')}}">
                        <i class="fas fa-plus mx-1" aria-hidden="true"></i>Add New Service
                    </a>
                </div>
            </div>
        </div>
        
        <div class="card-body px-0 pt-0 pb-4">
            <div class="table-responsive p-0 mt-4">
                <table class="table align-items-center" id="table-service">
                  <thead>
                      <tr>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Title</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Icon</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Description</th>
                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var table = $('#table-service').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('service.index') }}",
                language: {
                    paginate: {
                        next: '<i class="fa fa-angle-right"></i>',
                        previous: '<i class="fa fa-angle-left"></i>'  
                    }
                },
                columns: [
                    {
                        data: 'id', 
                        name: 'id',
                        render: function(data,type,full){
                            return full.DT_RowIndex;
                        }
                    },
                    {
                        data: 'title', 
                        name: 'title'},
                    {
                        data: 'icon', 
                        name: 'icon'
                    },
                    {
                        data: 'description', 
                        name: 'description',
                        render: function (data, type) {
                          return text_truncate(data,50);  
                        },
                    },
                    {data: 'action', name: 'action', orderable: true, searchable: true},
                ]
            });
        });
    </script>
@endpush
