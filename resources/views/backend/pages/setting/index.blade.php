@extends('backend.layouts.admin')

@section('title','Setting')

@section('content')

<div class="row">

  @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show text-white" role="alert">
            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
            <span class="alert-text"><strong>Message</strong></span>
            <p>{{$message}}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    
  <div class="card mt-4">
      <div class="card-header pb-0 p-3">
          <div class="row">
              <div class="col-6 d-flex align-items-center">
                  <h5 class="font-weight-bolder mb-0">Data Setting</h5>
              </div>
          </div>
      </div>
      
      <div class="card-body px-0 pt-0 pb-4">
        <form action="{{route('config-update')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
          @csrf
          <div class="table-responsive p-0 mt-4">
              <table class="table align-items-center" id="table-price">
                <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Title</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($settings as $key => $setting)
                        <tr>
                          <td>{{++$key}}</td>
                          <td>
                            <div class="ms-4">
                              @if ($setting->type == "file")
                                <div class="d-flex px-2 py-1">
                                  <div>
                                    <img src="{{Storage::url($setting->value)}}" alt="Image" class="avatar avatar-sm me-3">
                                  </div>
                                  <div class="ms-4">
                                    <h6 class="text-sm mb-0">{{$setting->title}}</h6>
                                    <p class="text-xs font-weight-bold mb-0">{{$setting->code}}</p>
                                  </div>
                                </div>
                              @else
                                <h6 class="text-sm mb-0">{{$setting->title}}</h6>
                                <p class="text-xs font-weight-bold mb-0">{{$setting->code}}</p>
                              @endif
                            </div>
                          </td>
                          <td>
                            <input type="hidden" name="id[]" value="{{$setting->id}}">
                            <input type="hidden" name="type[]" value="{{$setting->type}}">
                            <input type="hidden" name="code[]" value="{{$setting->code}}">
                            <input type="{{$setting->type}}" class="form-control" name="input-{{$setting->code}}" value="{{$setting->value}}">
                          </td>
                        </tr>
                    @endforeach
                  </tbody>
              </table>
          </div>
          <div class="row mx-2">
            <div class="form-group text-end">
                <button type="submit" class="btn btn-primary"><i class="fas fa-check mx-1" aria-hidden="true"></i>Save</button>
            </div>
          </div>
        </form>
      </div>
  </div>
</div>

@endsection
