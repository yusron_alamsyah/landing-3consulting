@extends('backend.layouts.admin')

@section('title','Add Testimonial')

@push('styles')
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.css" media="all" rel="stylesheet" type="text/css" />
@endpush

@section('content')

<div class="row">
    {{-- show error --}}
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show text-white" role="alert">
            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
            <span class="alert-text"><strong>Error!</strong></span>
            <p>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="card mt-4">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <div class="col-12 d-flex align-items-center">
                    <h5 class="font-weight-bolder mb-0">Form Testimoni</h5>
                </div>
            </div>
        </div>
        
        <div class="card-body px-0 pt-0 pb-4">
            <form action="{{route('testimoni.store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="row mx-2">
                    <div class="form-group">
                        <label class="form-control-label">Title</label>
                        <input class="form-control" type="text" value="{{old('title')}}" id="title" name="title">
                    </div>
                </div>
                <div class="row mx-2">
                    <div class="form-group">
                        <label class="form-control-label">Position</label>
                        <input class="form-control" type="text" value="{{old('position')}}" id="position" name="position">
                    </div>
                </div>
                <div class="row mx-2">
                    <div class="form-group">
                        <label class="form-control-label">Rating</label>
                        <input class="form-control" type="text" value="{{old('rating')}}" id="rating" name="rating">
                    </div>
                </div>
                <div class="row mx-2">
                    <div class="form-group">
                        <label class="form-control-label">Description</label>
                        <textarea class="form-control my-editor" id="my-editor" name="description" rows="10">{{old('description')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Image</label>
                    <input type="file" name="image" class="form-control">
                </div>
                <div class="row mx-2">
                    <div class="form-group text-end">
                        <a href="{{route('testimoni.index')}}" type="reset" class="btn btn-danger"><i class="fas fa-times mx-1" aria-hidden="true"></i>Cancel</a>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-check mx-1" aria-hidden="true"></i>Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/js/star-rating.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.js"></script>
    <script>
        $("#rating").rating({
            min:1, 
            max:5, 
            step:1, 
            size:'sm',
            showCaption:false,
        });
    </script>
@endpush