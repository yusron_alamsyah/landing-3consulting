<!-- Header Area wrapper Starts -->
<header id="header-wrap">
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <a href="index.html" class="navbar-brand"><img src="{{ Storage::url($conf_logo) }}" class="w-25" alt=""></a>       
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <i class="lni-menu"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
            <li class="nav-item {{  ($title == "Home") ? 'active' : '' }}">
                <?php if($title == "Home"){ ?>
                    <a class="nav-link" href="#hero-area">
                <?php }else{ ?>
                    <a class="nav-link" href="/">
                <?php } ?>
                Home
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#services">
                Services
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#pricing">
                Pricing
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#testimonial">
                Testimonial
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#contact">
                Contact
              </a>
            </li>
            <li class="nav-item {{  ($title == "News") ? 'active' : '' }}">
              <a class="nav-link" href="/news">
                News
              </a>
          </li>
            <li class="nav-item {{  ($title == "Career") ? 'active' : '' }}">
                <a class="nav-link" href="/career">
                  Career
                </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Navbar End -->

  </header>
  <!-- Header Area wrapper End -->