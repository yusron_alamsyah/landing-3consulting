<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="apple-touch-icon" sizes="76x76" href="{{ Storage::url($conf_logo) }}">
    <link rel="icon" type="image/png" href="{{ Storage::url($conf_logo) }}">
    <title>@yield('title') - {{$conf_title}}</title>
    <meta name="description" content="{{$conf_desc}}">
    <meta name="author" content="{{$conf_author}}">
    <meta name="keywords" content="{{$conf_keyword}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}" >
    <!-- Icon -->
    <link rel="stylesheet" href="{{ asset('frontend/fonts/line-icons.css') }}">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.theme.css') }}">
    
    <link rel="stylesheet" href="{{ asset('frontend/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/nivo-lightbox.css') }}">
    <!-- Animate -->
    <link rel="stylesheet" href="{{ asset('frontend/css/animate.css') }}">
    <!-- Main Style -->
    <link rel="stylesheet" href="{{ asset('frontend/css/main.css') }}">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">

  </head>
  <body>

    @include('frontend.layout.navbar')
    
    <div>
        @yield('content')
    </div>
    
    @include('frontend.layout.footer')
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('frontend/js/jquery-min.js') }}"></script>
    <script src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>            
    <script src="https://kit.fontawesome.com/020155b59b.js" crossorigin="anonymous"></script>
    <script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontend/js/wow.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.nav.js') }}"></script>
    <script src="{{ asset('frontend/js/scrolling-nav.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.counterup.min.js') }}"></script>      
    <script src="{{ asset('frontend/js/waypoints.min.js') }}"></script>   
    <script src="{{ asset('frontend/js/main.js') }}"></script>
      
  </body>
</html>
