@extends('frontend.layout.main')

@section('title',$detail->title)
    
@section('content')
    <div class="section-padding">
        <div class="container mt-5">
           <h5>{{ $detail->title }}</h5>
           <b>Created by : {{ $detail->user->name }}</b>
           <div>
                {!! $detail->desc !!}
           </div>
        </div>
    </div>
@endsection