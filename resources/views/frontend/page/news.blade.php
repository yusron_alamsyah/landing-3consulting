@extends('frontend.layout.main')

@section('title','News')
    
@section('content')
    <div class="section-padding" id="features">
        <div class="container">
            <div class="section-header pt-5">
                <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">News</h2>
                <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
            </div>
            <div class="row">
              <div class="col-6 offset-6">
                <form action="/news">
                  <div class="input-group">
                    <input name="search" value="{{ request('search') }}" type="search" class="form-control" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon2">
                  </div>
                </form>
              </div>
                @if ($listNews->count() > 0)
                    @foreach ($listNews as $item)
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="box-item p-0 wow fadeInLeft" data-wow-delay="0.3s">
                                <img style="width: 100%; height:200px" src="http://example.com/non-existent-image.jpg"
                                    onerror="this.onerror=null;this.src='https://deltaphilanthropies.org/wp-content/themes/delta_philanthropy/images/no-image-found-360x260.png';" />
                                <div class="p-3">
                                    <div class="text" style="height: 230px;">
                                        <h4>{{ $item->title }}</h4>
                                        {!! \Illuminate\Support\Str::limit($item->desc, 200, '...') !!}
                                        <div class="mt-3">
                                            <small>
                                                <i class="fa-solid fa-user mr-2"></i> By <a href=""
                                                    class="text-decoration-none">{{ $item->user->name }}</a><br>
                                                <i class="fa-solid fa-clock mr-2"></i>
                                                {{ $item->created_at->diffForHumans() }}
                                            </small>
                                        </div>
                                    </div>
                                    <div class="mt-3 pt-3 pb-3 d-flex justify-content-end"
                                        style="border-top:1px solid #d5d4e9;">
                                        <a href="/detail-news/{{ $item->slug }}"><button type="button"
                                                class="btn btn-sm btn-common">Read More</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12 d-flex justify-content-end pt-5">
                        {{ $listNews->links() }}
                    </div>
                @else
                    <div class="col-12 d-flex justify-content-start pt-5">
                        <h5>Data Not Found...</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <style>
        .shape {
            margin: 0 !important;
        }

        .page-item.active .page-link {
            border-color: #F63854;
            background-color: #F63854;
        }

        .page-link {
            color: #F63854;
        }
    </style>
@endsection
