@extends('frontend.layout.main')

@section('title','Home')
    
@section('content')
    <!-- Hero Area Start -->
    <div id="hero-area" class="hero-area-bg">
      <div class="container">      
        <div class="row">
          <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
            <div class="contents">
              <h2 class="head-title">{{$header->title}}</h2>
              <p>{{strip_tags($header->description)}}</p>
              <div class="header-button">
                <a rel="nofollow" href="#" class="btn btn-common">Purchase Now</a>
                <a href="https://www.youtube.com/watch?v=r44RKWyfcFw" class="btn btn-border video-popup">Intro Video</a>
              </div>
            </div>
          </div>
          <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
            <div class="intro-img">
              <img class="img-fluid" src="frontend/img/intro-mobile.png" alt="">
            </div>            
          </div>
        </div> 
      </div> 
    </div>
    <!-- Hero Area End -->
     <!-- Services Section Start -->
     <section id="services" class="section-padding">
        <div class="container">
          <div class="section-header text-center">
            <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Our Services</h2>
            <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
          </div>
          <div class="row">
            @foreach ($services as $key => $service)
              <!-- Services item -->
              <div class="col-md-6 col-lg-4 col-xs-12">
                <div class="services-item wow fadeInRight" data-wow-delay="{{0.3*++$key}}s">
                  <div class="icon">
                    <i class="{{$service->icon}}"></i>
                  </div>
                  <div class="services-content">
                    <h3><a href="#">{{$service->title}}</a></h3>
                    <p>{{strip_tags($service->description)}} </p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </section>
      <!-- Services Section End -->
  
      <!-- About Section start -->
      <div class="about-area section-padding bg-gray">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-12 col-xs-12 info">
              <div class="about-wrapper wow fadeInLeft" data-wow-delay="0.3s">
                <div>
                  <div class="site-heading">
                    <p class="mb-3">Manage Statistics</p>
                    <h2 class="section-title">{{$statistic->title}}</h2>
                  </div>
                  <div class="content">
                    <p>
                      {{strip_tags($statistic->description)}}
                    </p>
                    <a href="#" class="btn btn-common mt-3">Read More</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-12 col-xs-12 wow fadeInRight" data-wow-delay="0.3s">
              <img class="img-fluid" src="frontend/img/about/img-1.png" alt="" >
            </div>
          </div>
        </div>
      </div>
      <!-- About Section End -->
  
      <!-- Features Section Start -->
      <section id="features" class="section-padding">
        <div class="container">
          <div class="section-header text-center">          
            <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Awesome Features</h2>
            <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="content-left">
                @foreach ($features as $key => $feature)
                  @if ($loop->even)
                    <div class="box-item wow fadeInLeft" data-wow-delay="{{0.1*(++$key)}}s">
                      <span class="icon">
                        <i class="{{$feature->icon}}"></i>
                      </span>
                      <div class="text">
                        <h4>{{$feature->title}}</h4>
                        <p>{{strip_tags($feature->description)}}</p>
                      </div>
                    </div>
                  @endif
                @endforeach
              </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="show-box wow fadeInUp" data-wow-delay="0.3s">
                <img src="frontend/img/feature/intro-mobile.png" alt="">
              </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
              <div class="content-right">
                @foreach ($features as $key => $feature)
                  @if ($loop->odd)
                    <div class="box-item wow fadeInLeft" data-wow-delay="{{0.1*($key)}}s">
                      <span class="icon">
                        <i class="{{$feature->icon}}"></i>
                      </span>
                      <div class="text">
                        <h4>{{$feature->title}}</h4>
                        <p>{{strip_tags($feature->description)}}</p>
                      </div>
                    </div>
                  @endif
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Features Section End -->   
  
      <!-- Call To Action Section Start -->
      <section id="cta" class="section-padding">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">           
              <div class="cta-text">
                <h4>You're Using Free Lite Version</h4>
                <h5>Please purchase full version of the template to get all features and facilities</h5>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
              </br><a rel="nofollow" href="https://rebrand.ly/fusion-gg" class="btn btn-common">Purchase Now</a>
            </div>
          </div>
        </div>
      </section>
      <!-- Call To Action Section Start -->
  
      <!-- Pricing section Start --> 
      <section id="pricing" class="section-padding">
        <div class="container">
          <div class="section-header text-center">          
            <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Pricing</h2>
            <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
          </div>
          <div class="row">
            @foreach ($prices as $key => $price)
              <div class="col-lg-4 col-md-6 col-xs-12 @if ($key == 1) {{"active"}}  @endif">
                <div class="table wow fadeInUp" data-wow-delay="1.2s">
                  <div class="icon-box">
                    <i class="{{$price->icon}}"></i>
                  </div>
                  <div class="pricing-header">
                    <p class="price-value">${{$price->price}}<span> /mo</span></p>
                  </div>
                  <div class="title">
                    <h3>{{$price->title}}</h3>
                  </div>
                  <ul class="description">
                    {!! $price->description !!}
                  </ul>
                  <button class="btn btn-common">Purchase Now</button>
                </div> 
              </div>
            @endforeach
          </div>
        </div>
      </section>
      <!-- Pricing Table Section End -->
    
      <!-- Testimonial Section Start -->
      <section id="testimonial" class="testimonial section-padding">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div id="testimonials" class="owl-carousel wow fadeInUp" data-wow-delay="1.2s">
                @foreach ($testimonials as $testimonial)
                  <div class="item">
                    <div class="testimonial-item">
                      <div class="img-thumb">
                        <img src="{{Storage::url($testimonial->image)}}" alt="">
                      </div>
                      <div class="info">
                        <h2><a href="#">{{$testimonial->title}}</a></h2>
                        <h3><a href="#">{{$testimonial->position}}</a></h3>
                      </div>
                      <div class="content">
                        <p class="description">{{$testimonial->description}}</p>
                        <div class="star-icon mt-3">
                          @for ($i = 1; $i <= 5; $i++)
                              @if ($i <= $testimonial->rating)
                                <span><i class="lni-star-filled"></i></span>
                              @else
                                <span><i class="lni-star-half"></i></span>
                              @endif
                          @endfor
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </section>
  
      <!-- Testimonial Section End -->
  
      <!-- Call To Action Section Start -->
      <section id="cta" class="section-padding">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">           
              <div class="cta-text">
                <h4>Get 30 days free trial</h4>
                <p>Praesent imperdiet, tellus et euismod euismod, risus lorem euismod erat, at finibus neque odio quis metus. Donec vulputate arcu quam. </p>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
              </br><a href="#" class="btn btn-common">Register Now</a>
            </div>
          </div>
        </div>
      </section>
      <!-- Call To Action Section Start -->
  
      <!-- Contact Section Start -->
      <section id="contact" class="section-padding bg-gray">    
        <div class="container">
          <div class="section-header text-center">          
            <h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">Countact Us</h2>
            <div class="shape wow fadeInDown" data-wow-delay="0.3s"></div>
          </div>
          <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.3s">   
            <div class="col-lg-7 col-md-12 col-sm-12">
      <!-- Call To Action Section Start -->
      <section id="cta" class="section-padding">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">           
              <div class="cta-text">
                <h4>{{$contact->title}}</h4>
                <h5>{{strip_tags($contact->description)}}</h5>
                <br>
                <a rel="nofollow" href="#" class="btn btn-common">Purchase Now</a>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 text-right wow fadeInRight" data-wow-delay="0.3s">
            </div>
          </div>
        </div>
      </section>
      <!-- Call To Action Section Start -->
            </div>
            <div class="col-lg-5 col-md-12 col-xs-12">
              <div class="map">
                <object style="border:0; height: 280px; width: 100%;" data="{{$conf_maps}}"></object>
              </div>
            </div>
          </div>
        </div> 
      </section>
      <!-- Contact Section End -->
@endsection