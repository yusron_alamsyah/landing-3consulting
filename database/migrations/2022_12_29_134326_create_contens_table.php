<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 100);
            $table->longText('description');
            $table->string('icon', 20);
            $table->string('tipe', 20);
            $table->integer('price');
            $table->text('image');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contens');
    }
}
