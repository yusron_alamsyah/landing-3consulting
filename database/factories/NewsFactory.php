<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "title" => $this->faker->sentence(mt_rand(2,5)),
            "slug" => $this->faker->slug,
            "desc" => $this->faker->paragraph(mt_rand(5,10)),
            "created_by" => mt_rand(1,5)
        ];
    }
}
