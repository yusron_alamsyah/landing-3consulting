<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\CareerController;

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\SettingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class,'index']);
Route::get('/career', [CareerController::class,'index']);
Route::get('/news', [NewsController::class,'index']);
Route::get('/detail-news/{news:slug}', [NewsController::class,'showDetail']);

Auth::routes([
    'verify' => true
]);

Route::prefix('admin')->namespace('Admin')->middleware('auth')->group(function(){
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('/content', '\App\Http\Controllers\Admin\ContentController');
    Route::resource('/service', '\App\Http\Controllers\Admin\ServiceController');
    Route::resource('/feature', '\App\Http\Controllers\Admin\FeatureController');
    Route::resource('/price', '\App\Http\Controllers\Admin\PriceController');
    Route::resource('/testimoni', '\App\Http\Controllers\Admin\TestimoniController');
    Route::resource('/news', '\App\Http\Controllers\Admin\NewsController');

    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::put('/profile/{id}', [ProfileController::class, 'update'])->name('profile-update');

    Route::get('/configuration', [SettingController::class, 'index'])->name('config');
    Route::post('/configuration', [SettingController::class, 'update'])->name('config-update');

});
