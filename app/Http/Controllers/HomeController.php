<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Testimonial;

class HomeController extends Controller
{
    public function index(){
        $header = Content::where('tipe','header')->firstOrFail();
        $statistic = Content::where('tipe','statistic')->firstOrFail();
        $contact = Content::where('tipe','contact')->firstOrFail();
        $services = Content::all()->where('tipe','service');
        $features = Content::all()->where('tipe','feature');
        $prices = Content::all()->where('tipe','price');
        $testimonials = Testimonial::all();

        return view('frontend/page/home',[
            "title" => "Home",
            "header" => $header,
            "statistic" => $statistic,
            "contact" => $contact,
            "services" => $services,
            "features" => $features,
            "prices" => $prices,
            "testimonials" => $testimonials,
        ]);
    }
}
