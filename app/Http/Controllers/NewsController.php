<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    //
    public function index(){

        $news = News::latest()->filterSearch(request("search"));
   
        return view('frontend/page/news',[
            "title" => "News",
            "listNews" => $news->paginate(9)->withQueryString()
            // "listNews" => $news->get()
            // "listNews" => News::latest()->get()
            // "listNews" => News::all()
            // "listNews" => News::with("user")->get()
        ]);
    }

    public function showDetail(News $news){
        // return dd($news);
        return view('frontend/page/detailnews',[
            "title" => "Detail News",
            // "detail" => $news
            "detail" => $news->load("user")
        ]);
    }
}
