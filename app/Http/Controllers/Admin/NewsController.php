<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\News;
use DataTables;
use Auth;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = News::select(['id','title','desc']);
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="'.route('news.edit',$row->id).'" class="btn btn-icon btn-2 btn-primary">
                                    <span class="btn-inner--icon"><i class="fas fa-pencil"></i></span>
                                </a>
                                <form action="'.route('news.destroy',$row->id).'" class="d-inline" method="POST" autocomplete="off">
                                    '.csrf_field().'
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-icon btn-2 btn-danger" type="submit">
                                        <span class="btn-inner--icon"><i class="fas fa-close"></i></span>
                                    </button>
                                </form>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('backend.pages.news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'desc' => 'required',
        ]);
        
        $data = $request->all();
        $data['slug'] = Str::slug($request->title);
        $data['created_by'] = Auth::user()->id;

        News::create($data);
        return redirect()->route('news.index')->with(['success' => 'Successfully saved data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = News::findOrFail($id);
        return view('backend.pages.news.edit',[
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required',
            'desc' => 'required',
        ]);
        
        $data = $request->all();

        $item = News::findOrFail($id);
        $item->update($data);

        return redirect()->route('news.index')->with(['success' => 'Successfully updated data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = News::findOrFail($id);
        $item->delete();
        return redirect()->route('news.index')->with(['success' => 'Successfully deleted data']);
    }
}
