<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Testimonial;
use App\Models\News;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $service_total = Content::where('tipe','service')->count();
        $feature_total = Content::where('tipe','feature')->count();
        $testimoni_total = Testimonial::all()->count();
        $news_total = News::all()->count();

        $news_recent = News::limit(5)->orderByDesc('created_at')->get();
        $services = Content::all()->where('tipe','service');

        return view('backend.pages.dashboard',[
            'service_total' => $service_total,
            'feature_total' => $feature_total,
            'testimoni_total' => $testimoni_total,
            'news_total' => $news_total,
            'news_recent' => $news_recent,
            'services' => $services,
        ]);
    }
}
