<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;


class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $settings = Setting::all();

        return view('backend.pages.setting.index',['settings' => $settings]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();

        foreach ($data['id'] as $key => $value) {
            $update = [];
            if($data['type'][$key] == 'file'){
                if ($request->hasFile('input-'.$data['code'][$key])) {
                    $update['value']  = $request->file('input-'.$data['code'][$key])->store(
                        'assets/setting/','public'
                    );
                }
            }else{
                $update['value'] = $data['input-'.$data['code'][$key]];
            }

            $item = Setting::findOrFail($value);
            $item->update($update);
        }

        return redirect()->route('config')->with(['success' => 'Successfully updated data']);
    }
}
