<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;
use DataTables;

class TestimoniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Testimonial::select(['id','title','description','position','rating','image']);
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="'.route('testimoni.edit',$row->id).'" class="btn btn-icon btn-2 btn-primary">
                                    <span class="btn-inner--icon"><i class="fas fa-pencil"></i></span>
                                </a>
                                <form action="'.route('testimoni.destroy',$row->id).'" class="d-inline" method="POST" autocomplete="off">
                                    '.csrf_field().'
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-icon btn-2 btn-danger" type="submit">
                                        <span class="btn-inner--icon"><i class="fas fa-close"></i></span>
                                    </button>
                                </form>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('backend.pages.testimoni.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.testimoni.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'position' => 'required',
            'rating' => 'required',
            'description' => 'required',
            'image' => 'required|image',
        ]);
        
        $data = $request->all();

        // upload image
        $data['image']  = $request->file('image')->store(
            'assets/testimonial/','public'
        );

        Testimonial::create($data);
        return redirect()->route('testimoni.index')->with(['success' => 'Successfully saved data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Testimonial::findOrFail($id);
        return view('backend.pages.testimoni.edit',[
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required',
            'position' => 'required',
            'rating' => 'required',
            'description' => 'required',
            // 'image' => 'required|image',
        ]);
        
        $data = $request->all();

        if ($request->hasFile('image')) {
            // upload image
            $data['image']  = $request->file('image')->store(
                'assets/testimonial/','public'
            );
        }

        $item = Testimonial::findOrFail($id);
        $item->update($data);

        return redirect()->route('testimoni.index')->with(['success' => 'Successfully saved updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Testimonial::findOrFail($id);
        $item->delete();
        return redirect()->route('testimoni.index')->with(['success' => 'Successfully deleted data']);
    }
}
