<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Content;
use DataTables;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Content::select(['id','title','icon','description'])->where('tipe','service');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="'.route('service.edit',$row->id).'" class="btn btn-icon btn-2 btn-primary">
                                    <span class="btn-inner--icon"><i class="fas fa-pencil"></i></span>
                                </a>
                                <form action="'.route('service.destroy',$row->id).'" class="d-inline" method="POST" autocomplete="off">
                                    '.csrf_field().'
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-icon btn-2 btn-danger" type="submit">
                                        <span class="btn-inner--icon"><i class="fas fa-close"></i></span>
                                    </button>
                                </form>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        
        return view('backend.pages.service.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'icon' => 'required',
        ]);
        
        $data = $request->all();
        $data['tipe'] = 'service';

        Content::create($data);
        return redirect()->route('service.index')->with(['success' => 'Successfully saved data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Content::findOrFail($id);
        return view('backend.pages.service.edit',[
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'icon' => 'required',
        ]);
        
        $data = $request->all();

        $item = Content::findOrFail($id);
        $item->update($data);

        return redirect()->route('service.index')->with(['success' => 'Successfully updated data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Content::findOrFail($id);
        $item->delete();
        return redirect()->route('service.index')->with(['success' => 'Successfully deleted data']);
    }
}
