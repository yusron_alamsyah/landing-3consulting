<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;


class News extends Model
{
    use HasFactory;

    /**
     * The attributes thataren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ["id"];
    protected $with = ["user"];

    use SoftDeletes;

    protected $fillable = [
        'title',
        'slug',
        'desc',
        'created_by',
    ];

    public function scopeFilterSearch($query,$search){
        if($search ?? false){
            return  $query->where("title","like","%".$search."%")
            ->orWhere("desc","like","%".$search."%");
        }
    }

    public function user(){
        return $this->belongsTo(User::class,"created_by");
    }
}
